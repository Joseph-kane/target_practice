﻿using UnityEngine;

public class AlwaysFaceCamera : MonoBehaviour
{
	void Update ()
    {
        transform.LookAt(Camera.main.transform);
    }
}
