﻿using UnityEngine;
using System.Collections;

public class LSDECamera : MonoBehaviour
{
    [HideInInspector]
    public Transform bulletTransform;
    [HideInInspector]
    public Vector3 offset = new Vector3(0, 0, 0);

    void Start ()
    {
	    
	}

    void LateUpdate()
    {
        if (bulletTransform == null)
        {
            Destroy(gameObject);
        }
        else
        {
            transform.position = bulletTransform.position;
            transform.Translate(offset);
                //bulletTransform.TransformPoint(offset*(transform. bulletTransform.localScale));
        }
    }
}
