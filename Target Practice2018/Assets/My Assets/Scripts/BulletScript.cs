﻿using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public int bulletSpeed = 1;
    public int destroyAfterTime = 10;
    public float bulletSpread;

    public float spinningParticleSpeed = 1;

    public GameObject player;
    public Vector3 posLastFixedFrame; //The position of the object during the last fixedUpdate

    public BulletHoleDecalPoolScript bHDPoolScript;

	void Start () 
    {
        bHDPoolScript = GameObject.Find("BHDecalPool").GetComponent<BulletHoleDecalPoolScript>();
        player = GameObject.FindGameObjectWithTag("Player");
        
        //Note: At the start the "position last fixed frame" will be set to the current position, otherwise the collision check will
        //      sometimes falsely return true when the object is created
        posLastFixedFrame = transform.position;

        Destroy(gameObject, destroyAfterTime); //Destroy after specified time

        transform.Rotate(Random.Range(-bulletSpread, bulletSpread), Random.Range(-bulletSpread, bulletSpread), 0); //Give the bullet some inaccuracy

        GetComponent<Rigidbody>().AddForce(transform.forward * bulletSpeed); //Move the Bullet at set speed

        GetComponentInChildren<RotateConstantlyZ>().rotateSpeed = spinningParticleSpeed; //Add spin
    }
    
    void FixedUpdate()
    {
        //Check for bullet collisions by checking for collider between the position last frame and the position this frame
            //NOTE: on collison enter cant be used due to the fact this object shouldnt be hitting off objects and ontrigger cant be used
            //because it doesnt have a way to get info about the collision 
        RaycastHit hit;
        if (Physics.Linecast(posLastFixedFrame, transform.position, out hit) && hit.collider != GetComponent<Collider>() && hit.collider != player.GetComponent<CharacterController>())
        {
            bHDPoolScript.CreateBHDecal(hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal), hit.transform.gameObject); //Create a bullet hole
            
            //Target hit
            if (hit.collider.CompareTag("CircleTarget"))
            {
                Destroy(hit.collider.gameObject);
            }

            Destroy(this.gameObject); //Destroy after hitting anything

        }
        posLastFixedFrame = transform.position;//Store the positon this frame that will be used in the above in the next frame
    }
}
