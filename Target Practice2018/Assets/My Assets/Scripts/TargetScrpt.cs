﻿using UnityEngine;
using System.Collections;

public class TargetScrpt : MonoBehaviour
{
    GameController gameControllerScript;
    bool appQuitting= false;

    void Start()
    {
        gameControllerScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }


    void OnApplicationQuit()
    {
        appQuitting = true;
    }


    void OnDestroy()
    {
        gameControllerScript.targetsRemaining--;

        if(!appQuitting)
            Instantiate(gameControllerScript.brokenTargetPrefab, transform.position, transform.rotation);
    }
}
