﻿using UnityEngine;

public class BulletHoleDecalScript : MonoBehaviour 
{
    [Tooltip("How far above the surface the decal will be")]
    public float offset = 0.1f;
    [Tooltip("How much the new decal will be rotated on the y axis")]
    public float rotYOffset = 180;

    //Stores the default world scale of the bullet hole, set on its prefab
    private Vector3 defaultWorldScale;

    void Start () 
    {
        //Disabled on start because of object pooling
        gameObject.SetActive(false);

        //Store the default world scale of the bullet hole
        defaultWorldScale = transform.localScale;
	}

    /// <summary>
    /// Spawns this decal from the object pool by activating it and setting its new transform values
    /// </summary>
    /// <param name="pos">New position of the bullet hole decal</param>
    /// <param name="rot">New rotation of the bullet hole decal</param>
    /// <param name="hitGameObject">The gameobject to attach the bullet hole to</param>
    public void Create(Vector3 pos, Quaternion rot, GameObject hitGameObject)
    {
        gameObject.SetActive(true);
        
        transform.position = pos;
        transform.rotation = rot;
        transform.Translate(0, 0, offset);
        transform.Rotate(0, 180, 0);

        //The parenting is removed so that the scale can be reset cleanly and so thaty the scaling wont mess up when reparented
        transform.SetParent(null, true);
        transform.localScale = defaultWorldScale;

        //Assign the new parent to attach the bullet hole to
        transform.SetParent(hitGameObject.transform, true);

    }
}
