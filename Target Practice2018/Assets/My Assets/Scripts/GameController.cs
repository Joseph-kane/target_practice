﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{
    public bool shootingAllowed = true;
    public float timer = 0;
    public int targetsRemaining = 0;
    public int round = 0;
    public int roundTextDisplayTime = 2;
    public Text timerText;
    public Text targetsText;
    public Text roundText;
    public ChangeWeapons changeWeaponsScript;
    public GameObject brokenTargetPrefab;

    bool nextRoundMethodRunning = false;

    public List<GameObject> targetRoundsList; //Stores targets Round groups

    void Start ()
    {
	}
	
	void Update ()
    {
        if (!nextRoundMethodRunning)
        {
            //check remaining targets
            if (targetsRemaining <= 0)
            {
                if (round < 4)
                {
                    //NextRound
                    StartCoroutine(NextRound());
                }
                else
                {
                    timerText.text = "Time: " + timer;
                    targetsText.text = "Targets Left:" + targetsRemaining;

                    roundText.gameObject.SetActive(true);
                    roundText.text = "Finished";
                }
            }
            else
            {
                timer += Time.deltaTime;
                timerText.text = "Time: " + timer;

                targetsText.text = "Targets Left:" + targetsRemaining;
            }
        }
    }

    public IEnumerator NextRound()
    {
        nextRoundMethodRunning = true;
        shootingAllowed = false;

        changeWeaponsScript.changeWeapon();

        round++;

        //Display next round
        roundText.gameObject.SetActive(true);
        roundText.text = changeWeaponsScript.weaponsList[round].name + " Round";
        yield return new WaitForSeconds(roundTextDisplayTime);
        roundText.gameObject.SetActive(false);

        

        //Spawn New Targets
        targetRoundsList[round-1].SetActive(true);

        targetsRemaining = 8;

        shootingAllowed = true;
        nextRoundMethodRunning = false;
    }
}
