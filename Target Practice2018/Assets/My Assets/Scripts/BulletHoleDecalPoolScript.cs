﻿//This script stores a pool of BulletHoles which is much faster than having them constantly reinstantiated and destroyed over and over again
//Instead of being instantiated they are enabled and moved to there new position. Instead of being destroyed they are just disabled.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletHoleDecalPoolScript : MonoBehaviour 
{
    public int max = 100;
    public int current = 0;
    int loopsDone;
    public GameObject orginalBHD;
    public GameObject clone;
    public List<GameObject> bHDList;
    public List<BulletHoleDecalScript> bHDScriptList;


	void Awake ()
    {
        //fill pool
        for (int i=0; i<max; i++)
        {
            clone = (GameObject)Instantiate(orginalBHD);
            bHDList.Add(clone);
            bHDScriptList.Add(clone.GetComponent<BulletHoleDecalScript>());
            clone.name = "BHDecal" + (i+1);
            clone.transform.parent = transform;
        }
	}

    public void CreateBHDecal(Vector3 pos, Quaternion rot, GameObject hitGameObject)
    {
        CheckDestoyed(); //Make sure the decal hasnt been destroyed due to a destroyed parent

        //Check for inactive objects to use
        loopsDone = 0;
        while (bHDList[current].activeInHierarchy == true && loopsDone != max)
        {
            IncreaseCurrent();
            CheckDestoyed(); //Make sure the decal hasnt been destroyed due to a destroyed parent
            loopsDone++;
        }
        bHDScriptList[current].Create(pos, rot, hitGameObject);
        IncreaseCurrent();
    }

    public void IncreaseCurrent()
    {
        if (current < max-1)
            current++;
        else
            current = 0;
    }

    //Checks incase a decal has been destroyed, and if so recreates it
    public void CheckDestoyed()
    {
        if (bHDList[current] == null)
        {
            clone = (GameObject)Instantiate(orginalBHD);
            bHDList[current] = clone;
            bHDScriptList[current] = clone.GetComponent<BulletHoleDecalScript>();
            clone.name = "BHDecal" + (current + 1);
            clone.transform.parent = transform;
        }
    }
}