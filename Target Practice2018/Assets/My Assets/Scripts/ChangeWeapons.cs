﻿//Changes the curent weapon

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ChangeWeapons : MonoBehaviour 
{
    public List<GameObject> weaponsList; //Stores all Guns
    public Text curWeaponText;
    public int curWeapon = 0;

    //Change to next weapon
    public void changeWeapon()
    {
        weaponsList[curWeapon].SetActive(false); //Disable current Weapon
        curWeapon++;

        if (curWeapon >= weaponsList.Count)
            Debug.LogError("Tried to move to next weapon but reached end of weapon array");

        weaponsList[curWeapon].SetActive(true); //Enable Next Weapon
        curWeaponText.text = "Current Weapon: " + weaponsList[curWeapon].name;//Display Current weapon name in UI
    }

}
