﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class GunController : MonoBehaviour
{
    //Gun Stats
    public bool automatic;
    public float refireTime = 1; //Time until the gun can be shot again
    public float bulletSpread;
    public int magazineSize = 10;
    public double reloadTime = 1;
    public bool scoped = false;
    public float scopedBulletSpread;

    public enum ShotType {standard, shotgun}
    [Tooltip("What way the gun will shoot bullets\nStandard:One bullet at a time\nShotgun: A burst of bullets all at once")]
    public ShotType shotType = ShotType.standard;
    public int shotgunPellets = 10;
    
    bool canRefire = true; //True when the gun can shoot a bullet again
    bool isReloading = false; //True when gun is being reloaded
    int magAmmo;

    [Header("Last shot Dramatic effect")]
    public bool lastShotDramaticEffect = false;
    bool lastShotEffectStarted = false;
    public float timeSlow = 0.1f;
    RaycastHit lastShotEffectHit;
    public Camera lSDECameraPrefab;
    Camera lSDECamera;
    public Vector3 lSDECameraDisplacement;
    public Transform lSDECameraFollowTransform;
    public GameObject lSParticleSystemPrefab;
    GameObject lSParticleSystem;

    [Header("Input")]
    public KeyCode shootButton = KeyCode.Mouse0;
    public KeyCode reloadButton = KeyCode.Space;
    public KeyCode scopeButton = KeyCode.Mouse1;

    [Header("References")]
    public GameObject bulletPrefab;
    GameObject bulletClone;
    BulletScript bulletCloneScript;
    public GameController gameControllerScript;
    public Slider gunReloadSlider;
    public Text ammoText;
    public Image crosshairImage;
    public Image scopeCrosshairImage;

    void Start () 
    {
        magAmmo = magazineSize;
        ammoText.text = ""+ magAmmo;
    }

    void Update()
    {
        if (!isReloading)
        {
            if (canRefire && gameControllerScript.shootingAllowed)
            {
                if (magAmmo > 0)
                {
                    if (automatic)
                    {
                        if (Input.GetKey(shootButton) && canRefire && gameControllerScript.shootingAllowed)
                            StartCoroutine(ShootGun());
                    }
                    else
                    {
                        if (Input.GetKeyDown(shootButton) && canRefire && gameControllerScript.shootingAllowed)
                            StartCoroutine(ShootGun());
                    }
                }
                else
                {
                    StartCoroutine(ReloadMag());
                }
            }

            if (Input.GetKeyDown(reloadButton))
            {
                StartCoroutine(ReloadMag());
            }
        }

        if(scoped)
        {
            if (Input.GetKey(scopeButton))
            {
                scopeCrosshairImage.gameObject.SetActive(true);
                crosshairImage.gameObject.SetActive(false);
                Camera.main.fieldOfView = 25;
            }
            else
            {
                scopeCrosshairImage.gameObject.SetActive(false);
                crosshairImage.gameObject.SetActive(true);
                Camera.main.fieldOfView = 70;
            }
        }
    }

    public void OnEnable()
    {
        canRefire = true;
        ammoText.text = "" + magAmmo;
    }

    public void OnDisable()
    {
        isReloading = false;

        if(gunReloadSlider != null)
            gunReloadSlider.gameObject.SetActive(false);

        if (lastShotEffectStarted)
        {
            Time.timeScale = 1;
            Time.fixedDeltaTime *= 1/timeSlow;
        }
    }

    IEnumerator ShootGun()
    {
        canRefire = false;

        if (shotType == ShotType.standard)
        {
            bulletClone = (GameObject)Instantiate(bulletPrefab, transform.position, transform.rotation);
            bulletCloneScript = bulletClone.GetComponent<BulletScript>();

            //Assign bullet stats from gun
            if(Input.GetKey(scopeButton))
                bulletCloneScript.bulletSpread = scopedBulletSpread;
            else
                bulletCloneScript.bulletSpread = bulletSpread;

            magAmmo--;
            ammoText.text = "" + magAmmo;
        }
        else if (shotType == ShotType.shotgun)
        {
            for (int i = 0; i < shotgunPellets; i++)
            {
                bulletClone = (GameObject)Instantiate(bulletPrefab, transform.position, transform.rotation);
                bulletCloneScript = bulletClone.GetComponent<BulletScript>();

                //Assign bullet stats from gun
                bulletCloneScript.bulletSpread = bulletSpread;
            }

            magAmmo--;
            ammoText.text = "" + magAmmo;
        }

        //Dramatic effect for last shot
            //check if its the final shot
        if (lastShotDramaticEffect && gameControllerScript.targetsRemaining == 1 && Physics.Raycast(transform.position, transform.forward, out lastShotEffectHit) && lastShotEffectHit.collider.tag == "CircleTarget" && !lastShotEffectStarted)
        {
            lastShotEffectStarted = true;
            Time.timeScale = timeSlow;
            Time.fixedDeltaTime *= timeSlow;
            lSDECameraFollowTransform = bulletClone.transform;
            lSDECamera = (Camera)Instantiate(lSDECameraPrefab, bulletClone.transform, false);
            lSDECamera.transform.Translate(lSDECameraDisplacement, lSDECameraFollowTransform);
            lSDECamera.transform.LookAt(lSDECameraFollowTransform);
            lSDECamera.GetComponent<DepthOfField>().focalTransform = lSDECameraFollowTransform;

            lSParticleSystem = (GameObject)Instantiate(lSParticleSystemPrefab, bulletClone.transform);
            lSParticleSystem.transform.rotation = bulletClone.transform.rotation;

            //CHange last bullet fired to shoot in straight line to always hit target
            bulletCloneScript.bulletSpread = 0;
        }

        yield return new WaitForSeconds((float)refireTime);

        canRefire = true;
    }

    IEnumerator ReloadMag() //Displays a progress bar for gun reload
    {
        isReloading = true;

        //Display progress of reload
        gunReloadSlider.maxValue = (float)reloadTime;
        gunReloadSlider.value = (float)reloadTime;
        gunReloadSlider.gameObject.SetActive(true);
        while (gunReloadSlider.value > 0)
        {
            yield return null;
            gunReloadSlider.value -= Time.deltaTime;
        }
        gunReloadSlider.gameObject.SetActive(false);

        //Reload finished
        magAmmo = magazineSize;
        ammoText.text = "" + magAmmo;

        isReloading = false;
    }
}
